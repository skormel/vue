import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import mapboxgl from "mapbox-gl"; // or "const mapboxgl = require('mapbox-gl');"

mapboxgl.accessToken =
  "pk.eyJ1Ijoic2tvcm1lbCIsImEiOiJja3h0YTlyemUwaXpxMm5waHVkNDZzaGVuIn0.mcGnDijKVHGVu0zNEdOB9w";

if (!navigator.geolocation) {
  alert("Tu navegador no soporta GeoLocation");
  throw new Error("Tu navegador no soporta GeoLocation");
}

createApp(App).use(store).use(router).mount("#app");
