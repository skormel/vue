import { useMapStore, usePlacesStore } from "@/composables";
import Mapboxgl from "mapbox-gl";
import { defineComponent, onMounted, ref, watch } from "vue";

export default defineComponent({
  name: "MapView",
  setup() {
    const mapElement = ref<HTMLDivElement>();
    const { userLocation, isUserLocationReady } = usePlacesStore();
    const { setMap } = useMapStore();

    const initMap = async () => {
      if (!mapElement.value) throw new Error("Div Element no exists");
      if (!userLocation.value) throw new Error("user location no exists");

      await Promise.resolve();

      const map = new Mapboxgl.Map({
        container: mapElement.value, // container ID
        style: "mapbox://styles/mapbox/light-v10", // style URL
        center: userLocation.value, // starting position [lng, lat]
        zoom: 15, // starting zoom
      });

      // offset it's used to position
      const myLocationPopup = new Mapboxgl.Popup({
        offset: [0, -25],
      }).setLngLat(userLocation.value).setHTML(`
      <h4>WOOOOOLA</h4>
      <p>Aquí estoy</p>
      `);

      const myLocationMarker = new Mapboxgl.Marker()
        .setLngLat(userLocation.value)
        .setPopup(myLocationPopup)
        .addTo(map);

      setMap(map);
    };

    onMounted(() => {
      if (isUserLocationReady) {
        return initMap();
      }

      console.log("No tengo localización aún");
    });

    watch(isUserLocationReady, (newVal) => {
      if (isUserLocationReady.value) initMap();
    });

    return { isUserLocationReady, mapElement };
  },
});
